<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\TestController;
use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Access;
use App\Middleware\Registred;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\ModuleController;
//delimiter//

$c = $app->getContainer();

$app->get('/message/no-access', UserController::class.':noAccess')->setName('error.noAccess');

$app->get('/', function (Request $request, Response $response) use ($c) {
    return $this->view->render($response, 'admin/layout/layout.twig');
})->add(new Registred($container));

$app->get('/test', TestController::class.':test');

$app->group('/admin', function() use($app, $c) {

    $app->get('/users', UserController::class . ':showAdminUserList')->setName("user.showAdminUserList")->add(new Access($c, ['user_view']));
    $app->get('/users/add', UserController::class . ':showAdminUserAdd')->setName("user.showAdminUserAdd")->add(new Access($c, ['user_view', 'user_create']));
    $app->get('/users/{id}', UserController::class . ':showAdminUserEdit')->setName('user.showAdminUserEdit')->add(new Access($c, ['user_view', 'user_update']));

    $app->get('/roles', RoleController::class . ':showAdminRoleList')->setName('role.showAdminRoleList')->add(new Access($c, ['roles_view']));

    $app->get('/navigation', NavigationController::class . ':showAdminNavigationList')->setName('navigation.showAdminNavigationList');
    $app->get('/navigation/add', NavigationController::class . ':showAdminNavigationAdd')->setName('navigation.showAdminNavigationAdd');
    $app->get('/navigation/{id}', NavigationController::class . ':showAdminNavigationEdit')->setName('navigation.showAdminNavigationEdit');

    $app->get('/navigation/{id}/items', NavigationItemController::class.':showadminNavigationItemList')->setName('navigation.showAdminNavigationItemList');
    $app->get('/navigation/{id}/items/add', NavigationItemController::class.':showAdminNavigationItemAdd')->setName('navigation.showAdminNavigationItemAdd');
    $app->get('/navigation/{id}/items/{item_id}', NavigationItemController::class.':showAdminNavigationItemEdit')->setName('navigation.showAdminNavigationItemEdit');

    $app->get('/modules', ModuleController::class . ':showAdminModuleList')->setName('module.showAdminModuleList')->add(new Access($c, ['modules_view']));

})->add(new Registred($c));