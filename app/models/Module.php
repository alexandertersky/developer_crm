<?php

namespace App\Models;

class Module extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'modules';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
        'status' => 'tinyInteger',
        'path' => 'text'
	];

}