<?php

namespace App\Models;

class Navigation extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'navigation';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
	];

    public function items()
    {
        return $this->hasMany(NavigationItem::class, 'navigation_id', 'id');
    }

}