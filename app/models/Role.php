<?php

namespace App\Models;

use App\Models\User;

class Role extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'id' => 'integer',
		'slug' => 'string',
		'name' => 'string',
		'permissions' => 'text',
	];

    public function users() {
        return $this->belongsToMany(User::class, 'role_users',  'role_id', 'user_id');
    }
}