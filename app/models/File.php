<?php

namespace App\Models;

class File extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'files';
    protected $primaryKey = 'id';
    protected $checkbox = [];

    public function fileable()
    {
        return $this->morphTo();
    }
}