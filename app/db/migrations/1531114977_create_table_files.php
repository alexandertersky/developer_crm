<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_files_1531114977 {
    public function up() {
        Capsule::schema()->create('files', function($table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('md5_filename');
            $table->string('path');
            $table->string('extension');
            $table->string('size');
            $table->timestamp('created_at')->useCurrent();
        });
    }
}
