<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_navigation_1533530713 {
    public function up() {
        Capsule::schema()->create('navigation', function($table) {
            $table->increments('id');
            $table->string('title');
        });

        Capsule::table('navigation')->insert([
            'id' => 1,
            'title' => 'Стандартное'
        ]);
    }
}
