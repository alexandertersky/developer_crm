<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_throttle_1530860555 {
    public function up() {
        Capsule::schema()->create('throttle', function($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('type');
            $table->string('ip')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
