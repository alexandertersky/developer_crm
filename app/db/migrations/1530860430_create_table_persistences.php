<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_persistences_1530860430 {
    public function up() {
        Capsule::schema()->create('persistences', function($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('code');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
