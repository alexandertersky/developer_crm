<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_modules_1534302175 {
    public function up() {
        Capsule::schema()->create('modules', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('status')->default(0);
            $table->string('path');
            $table->text('convention');
        });

        
    }
}
