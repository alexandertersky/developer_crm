<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_reminders_1530860449 {
    public function up() {
        Capsule::schema()->create('reminders', function($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('code');
            $table->tinyInteger('completed')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
