$app->group('/api', function() use($app, $c) {

    $app->post('/${low_route}s', ${route}Controller::class.':create${route}')->setName('${low_route}.create${route}')->add(new Access($c, ['${low_route}s_create']));
    $app->post('/${low_route}s/{id}', ${route}Controller::class.':update${route}')->setName('${low_route}.update${route}')->add(new Access($c, ['${low_route}s_update']));
    $app->get('/${low_route}s/{id}/delete', ${route}Controller::class.':delete${route}')->setName('${low_route}.delete${route}')->add(new Access($c, ['${low_route}s_delete']));

})->add(new Registred($container));