<li>${class_name}</li>
<li>
    <ul>
        <li>
            <input type="hidden" name="${class_name_low}s_view" value="false">
            <input id="${class_name_low}s_view" type="checkbox" name="${class_name_low}s_view" value="true">
            <label class="noselect" for="${class_name_low}s_view">Просмотр</label>
        </li>
        <li>
            <input type="hidden" name="${class_name_low}s_create" value="false">
            <input id="${class_name_low}s_create" type="checkbox" name="${class_name_low}s_create" value="true">
            <label class="noselect" for="${class_name_low}s_create">Создание</label>
        </li>
        <li>
            <input type="hidden" name="${class_name_low}s_update" value="false">
            <input id="${class_name_low}s_update" type="checkbox" name="${class_name_low}s_update" value="true">
            <label class="noselect" for="${class_name_low}s_update">Обновление</label>
        </li>
        <li>
            <input type="hidden" name="${class_name_low}s_delete" value="false">
            <input id="${class_name_low}s_delete" type="checkbox" name="${class_name_low}s_delete" value="true">
            <label class="noselect" for="${class_name_low}s_delete">Удаление</label>
        </li>
    </ul>
</li>