<?php
use Faker\Factory;
use Illuminate\Database\Capsule\Manager as Capsule;

class ${class_name}
{
    public static function run($count = 5)
{
    $faker = Factory::create('Ru_RU');

    for ($i = 1; $i <= $count; $i++)
    {
        Capsule::table('${table_name}')->insert([${insert}]);
    }
}
}